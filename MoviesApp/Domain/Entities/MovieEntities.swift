//
//  MovieEntities.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation

public struct Movie {
    let isAdult: Bool
    let backdropPath: String?
    let genreIDs: [Int]
    let id: Int
    let originalLanguage: String
    let originalTitle: String
    let overview: String
    let popularity: Double
    let posterPath: String
    let releaseDate: String
    let title: String
    let video: Bool
    let voteAverage: Double
    let voteCount: Int
}
