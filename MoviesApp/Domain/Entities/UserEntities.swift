//
//  UserDomain.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation

struct UserEntities {
    let fullname: String
    let email: String
    let profileImage: Data
    let dateOfBirth: Date
}
