//
//  UserRepository.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift

protocol UserRepository {
    func loginSession() -> Completable
    func login(email: String, password: String) -> Single<UserEntities>
    func register(fullname: String,
                  email: String,
                  password: String,
                  dateBirth: Date,
                  profileImage: Data, completion: @escaping (Result<Bool?, CoreDataStorageError>) -> Void)
}
