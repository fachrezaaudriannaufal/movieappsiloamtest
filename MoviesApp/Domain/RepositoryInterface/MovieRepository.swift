//
//  MovieRepository.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift

protocol MovieRepository {
    func fetchPopularMoview() -> Single<[Movie]>
}
