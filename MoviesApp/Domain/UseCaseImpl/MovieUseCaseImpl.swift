//
//  MovieUseCaseImpl.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift

final class MovieUseCaseImpl: MovieUseCase {
    
    private let repository: MovieRepository
    
    init(repository: MovieRepository) {
        self.repository = repository
    }

    func fetchPopularMoview() -> Single<[Movie]> {
        return repository.fetchPopularMoview()
    }
}
