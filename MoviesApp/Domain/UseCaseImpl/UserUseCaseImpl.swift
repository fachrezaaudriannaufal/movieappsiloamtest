//
//  UserUseCaseImpl.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift

final class UserUseCaseImpl: UserUseCase {

    private let repository: UserRepository
    
    init(repository: UserRepository) {
        self.repository = repository
    }
    
    func loginSession() -> Completable {
        return repository.loginSession()
    }
    
    func login(email: String, password: String) -> Single<UserEntities> {
        return repository.login(email: email, password: password)
    }
    
    func getProfile() {
        debugPrint("Do getProfile")
    }

    func register(fullname: String,
                  email: String,
                  password: String,
                  dateBirth: Date,
                  profileImage: Data, completion: @escaping (Result<Bool?, CoreDataStorageError>) -> Void) {
        repository.register(fullname: fullname,
                            email: email,
                            password: password,
                            dateBirth: dateBirth,
                            profileImage: profileImage,
                            completion: { result in
            switch result {
            case let .success(data):
                completion(.success(data))
            case let .failure(error):
                completion(.failure(error))
            }
        })
    }
}
