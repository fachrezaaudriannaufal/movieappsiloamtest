//
//  MovieUseCase.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift

protocol MovieUseCase {
    func fetchPopularMoview() -> Single<[Movie]>
}
