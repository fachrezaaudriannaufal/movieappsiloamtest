//
//  MovieDIContainer.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation

final class MoviewDIContainer {
    static let shared = MoviewDIContainer()
    
    func makeMovieAccountUseCase() -> UserUseCase {
        return UserUseCaseImpl(repository: makeMovieAccountRepository())
    }

    func makeMovieAccountRepository() -> UserRepositoryImpl {
        return UserRepositoryImpl(coreData: CoreDataUserAccountStorage(), cache: CacheDataStorageResourceImpl())
    }
    
    func makeMovieUseCase() -> MovieUseCase {
        return MovieUseCaseImpl(repository: makeMovieRepository())
    }

    func makeMovieRepository() -> MovieRepositoryImpl {
        return MovieRepositoryImpl()
    }
}
