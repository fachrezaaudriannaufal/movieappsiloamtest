//
//  Essay.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation

/*
 1. Define and explain the meaning of Core Data and how it functions.
 Answer:
 Core Data is a permanent data for offline use (like cache) and core data can manage some object instances
 
 2. Explain the difference between Core Data and UserDefaults.
 Answer:
 
 - Core Data is also a persistence solution, but it was built with performance and flexibility in mind
 You can even keep the data in-memory or create a custom persistent store to fit your project's needs.
 Another key benefit of Core Data is its support for relational data sets and have an ability to query
 
 - UserDefaults is a class that allows for the simple storage of different data types. It is primarily used to store small amounts of data to persist between app launches or device restarts. UserDefaults can store basic types (bool, float, double, int, etc) and more complex types (array, dictionary). Though UserDefaults has no structure, it is said to be faster than core data because all it is is key value pairs. Since it has no structure, it is useful to store data that does not require structure
 
 3. When using Realm, failure to map models might result in migration failure. Explain how to
 prevent this and define what are the things expected to be stored in Realm.
 
 - Actually, i never user realm, but to prevent failure map models, you can use decodeIfPresent in the model.
 
 4. Define and explain how to prevent memory leakage in Swift.
 - You should see the reference (strong, weak, unowned). If you have strong - strong reference, the Automatic Counting Reference will going up and cant be release. So, what will we do? prevent the strong - strong refernce with weak.. and the counting reference will going to zero. Class can be deinit.
    
    If we want to check leaking memory, we can use instrument in xcode. By the way, if the memory leaked, it can make your app force close.
 
 5. Define and explain the Grand Central Dispatch technology.
 
    Grand Central Dispatch (GCD) is a low-level API for managing concurrent operations. It can help improve your app’s responsiveness by deferring computationally expensive tasks to the background. It’s an easier concurrency model to work with than locks and threads.
 
    GCD provides three main types of queues:

     Main queue: Runs on the main thread and is a serial queue.
     Global queues: Concurrent queues shared by the whole system. Four such queues exist, each with different priorities: high, default, low and background. The background priority queue has the lowest priority and is throttled in any I/O activity to minimize negative system impact.
     Custom queues: Queues you create that can be serial or concurrent. Requests in these queues end up in one of the global queues.
     
     The QoS classes are:
     User-interactive: This represents tasks that must complete immediately to provide a nice user experience. Use it for UI updates, event handling and small workloads that require low latency. The total amount of work done in this class during the execution of your app should be small. This should run on the main thread.
     
     User-initiated: The user initiates these asynchronous tasks from the UI. Use them when the user is waiting for immediate results and for tasks required to continue user interaction. They execute in the high-priority global queue.
     
     Utility: This represents long-running tasks, typically with a user-visible progress indicator. Use it for computations, I/O, networking, continuous data feeds and similar tasks. This class is designed to be energy efficient. This gets mapped into the low-priority global queue.
     
     Background: This represents tasks the user isn’t directly aware of. Use it for prefetching, maintenance and other tasks that don’t require user interaction and aren’t time-sensitive. This gets mapped into the background priority global queue.
 
 6. What is the limitation of SwiftUI? How does it differ with UIKit?
    - You should using iOS 13 to use Swift UI. But, many application have some users that still use below iOS 13. For now, i'm not prefer to change from UIKit to SwiftUI. We can't sacriface the users.
 
    SwiftUI -> We can directly see the UI Result, It’s easy to learn, and the code is simple and clean,
    UIKit -> We shoud run the project first, usually its takes time, Using SceneDelegate
 
    
 7. When growing capabilities of your application, size and performance usually became one
 thing that usually got forgotten. What can we do to keep your app size down and performance high while expanding your system feature.
 
    - Before we talk about size and performance, we should talk about the Documentation. Why? because its important, sometimes the new employee doesn't know what the code is. Only previous programmer and god will know.
    
        Okay, after that you can talk about the architecture. You should find the architecture that fit with your apps.
        If the architecture already fit and running well, use should concern about unit test, CI / CD, and the last thing is modular application.
 
        Oh, you can consider to reduce dependency with 3rd party, sometimes it makes stressed when the 3rd doesn't up to date / suddenly have the problem, it will impact to you apps.
 */
