//
//  MovieAPI.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import Moya

enum MovieAPI {
    case fetchPopularMoview(page: Int)
}

extension MovieAPI: TargetType {
    var baseURL: URL {
        switch self {
        case .fetchPopularMoview:
            return URL(string: "https://api.themoviedb.org/3")!
        }
    }
    
    var path: String {
        return "/movie/popular"
    }
    
    var method: Moya.Method {
        switch self {
        case .fetchPopularMoview:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Moya.Task {
        switch self {
        case let .fetchPopularMoview(page):
            let parameters: [String:String] = [
                "api_key" : "ed9d5af2f620bc920de2859d27746216",
                "page" : String(page)
            ]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
        }
    }

    var headers: [String : String]? {
        return nil
    }
    
    
}
