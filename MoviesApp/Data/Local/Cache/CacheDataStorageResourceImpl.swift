//
//  CacheDataStorageResourceImpl.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift

class CacheDataStorageResourceImpl: CacheDataStorage {
    func deleteCacheData(key: [UserDefaultCacheProviderKeys]) -> Completable {
        return Completable.create { observer in
            key.forEach {
                UserDefaults.standard.removeObject(forKey: $0.rawValue)
            }

            observer(.completed)
            return Disposables.create()
        }
    }

    func getCacheData<T>(key: UserDefaultCacheProviderKeys) -> Single<T> where T : Decodable, T : Encodable {
        return Single<T>.create { observer in
            if let data = UserDefaults.standard.object(forKey: key.rawValue) as? Data,
               let dataDecoded = try? JSONDecoder().decode(T.self, from: data) {
                observer(.success(dataDecoded))
            }

            return Disposables.create()
        }
    }

    func addCacheData<T>(value: T, key: UserDefaultCacheProviderKeys) -> Completable where T : Decodable, T : Encodable {
        return Completable.create { observer in
            do {
                let data = try JSONEncoder().encode(value)
                UserDefaults.standard.set(data, forKey: key.rawValue)
                observer(.completed)
            } catch let error {
                observer(.error(error))
            }

            return Disposables.create()
        }
    }
}
