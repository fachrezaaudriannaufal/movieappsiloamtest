//
//  CacheDataStorage.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift

enum UserDefaultCacheProviderKeys: String, CaseIterable {
    case isLoggedIn = "isLoggedIn"
}

protocol CacheDataStorage {
    func deleteCacheData(key: [UserDefaultCacheProviderKeys]) -> Completable
    func getCacheData<T: Codable>(key: UserDefaultCacheProviderKeys) -> Single<T>
    func addCacheData<T: Codable>(value: T, key: UserDefaultCacheProviderKeys) -> Completable
}
