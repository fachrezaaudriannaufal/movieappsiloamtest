//
//  CoreDataMoviesStorage.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import CoreData
import RxSwift

enum CoreDataStorageError: Error {
    case readError(Error)
    case saveError(Error)
    case deleteError(Error)
    case emptyData
}

protocol CoreDataAccountProtocol {
    func getResponse(email: String, password: String, completion: @escaping (Result<UserMoviesDTO?, CoreDataStorageError>) -> Void)
    func save(request: UserMoviesDTO, completion: @escaping (Result<Bool?, CoreDataStorageError>) -> Void)
}

final class CoreDataUserAccountStorage: CoreDataAccountProtocol {
    private let coreDataStorage: CoreDataStorage
    
    init(coreDataStorage: CoreDataStorage = CoreDataStorage.shared) {
        self.coreDataStorage = coreDataStorage
    }

    func getResponse(email: String, password: String, completion: @escaping (Result<UserMoviesDTO?, CoreDataStorageError>) -> Void) {
        coreDataStorage.performBackgroundTask { context in
            do {
                let userFetch: NSFetchRequest<User> = User.fetchRequest()
                userFetch.predicate = NSPredicate(
                    format: "email LIKE %@ AND password LIKE %@", email, password
                )

                if let results = try context.fetch(userFetch).first {
                    let userData = UserMoviesDTO(fullname: results.fullname,
                                                 email: results.email,
                                                 password: results.password,
                                                 profileImage: results.profile,
                                                 dateOfBirth: results.birth)
                    completion(.success(userData))
                } else {
                    completion(.failure(CoreDataStorageError.emptyData))
                }
            } catch {
                completion(.failure(CoreDataStorageError.readError(error)))
            }
        }
    }
    
    func save(request: UserMoviesDTO, completion: @escaping (Result<Bool?, CoreDataStorageError>) -> Void) {
        self.coreDataStorage.performBackgroundTask { context in
            do {
                guard let userEntityName =  NSEntityDescription.entity(forEntityName: "User", in: context) else { return }
                let user = User(entity: userEntityName, insertInto: context)

                user.fullname = request.fullname
                user.email = request.email
                user.password = request.password
                user.profile = request.profileImage
                user.birth = request.dateOfBirth
                
                try context.save()
                completion(.success(true))
            } catch let error {
                completion(.failure(.saveError(error)))
            }
        }
    }
}
