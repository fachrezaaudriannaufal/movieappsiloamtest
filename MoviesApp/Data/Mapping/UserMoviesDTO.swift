//
//  UserMoviesDTO.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift
import RxCocoa

struct UserMoviesDTO {
    let fullname: String?
    let email: String?
    let password: String?
    let dateOfBirth: Date?
    let profileImage: Data?
    
    enum CodingKeys: String, CodingKey {
        case fullname
        case email
        case dateOfBirth
        case password
        case profileImage
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        fullname = try container.decodeIfPresent(String.self, forKey: .fullname)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        dateOfBirth = try container.decodeIfPresent(Date.self, forKey: .dateOfBirth)
        password = try container.decodeIfPresent(String.self, forKey: .password)
        profileImage = try container.decodeIfPresent(Data.self, forKey: .profileImage)
    }

    init(fullname: String? = nil, email: String? = nil, password: String? = nil, profileImage: Data? = nil, dateOfBirth: Date? = nil) {
        self.fullname = fullname
        self.email = email
        self.password = password
        self.profileImage = profileImage
        self.dateOfBirth = dateOfBirth
    }
}

extension UserMoviesDTO {
    func toDomain() -> UserEntities {
        return .init(fullname: fullname ?? "",
                     email: email ?? "",
                     profileImage: profileImage ?? Data(),
                     dateOfBirth: dateOfBirth ?? Date())
    }
}
