//
//  MovieRepositoryImpl.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import Moya
import RxSwift

final class MovieRepositoryImpl: MovieRepository {
    
    private let moyaProvider = MoyaProvider<MovieAPI>()
    
    func fetchPopularMoview() -> Single<[Movie]> {
        return moyaProvider.rx.request(
                .fetchPopularMoview(page: 1))
                .map(PopularMoviesResponseDTO.self)
                .map {
                    $0.results.map { $0.toDomain()}
                }
    }
}
