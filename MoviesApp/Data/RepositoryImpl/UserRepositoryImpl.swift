//
//  UserRepositoryImpl.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift

final class UserRepositoryImpl: UserRepository {
    private let coreData: CoreDataAccountProtocol
    private let cache: CacheDataStorage
    
    init(coreData: CoreDataAccountProtocol, cache: CacheDataStorage) {
        self.coreData = coreData
        self.cache = cache
    }

    func loginSession() -> Completable {
        return cache.addCacheData(value: true, key: .isLoggedIn)
    }

    func login(email: String, password: String) -> Single<UserEntities> {
        return Single<UserEntities>.create { observer in
            self.coreData.getResponse(email: email, password: password, completion: { result in
                switch result {
                case let .success(response):
                    guard let response = response else { return }
                    observer(.success(response.toDomain()))
                case let .failure(error):
                    observer(.error(error))
                }
            })
            return Disposables.create()
        }
    }

    func register(fullname: String, email: String, password: String, dateBirth: Date, profileImage: Data, completion: @escaping (Result<Bool?, CoreDataStorageError>) -> Void) {
        let request = UserMoviesDTO(fullname: fullname, email: email, password: password, profileImage: profileImage)
        self.coreData.save(request: request, completion: { result in
            switch result {
            case .success:
                completion(.success(true))
            case let .failure(error):
                completion(.failure(error))
            }
        })
    }
}
