//
//  HomeViewModel.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import Foundation
import RxSwift
import RxCocoa

final class HomeViewModel {
    
    private let useCase: MovieUseCase
    private let disposeBag = DisposeBag()
    
    let movies = BehaviorRelay<[Movie]>(value: [])
    let errors = PublishSubject<Error>()
    let isLoading = PublishSubject<Bool>()

    init(useCase: MovieUseCase) {
        self.useCase = useCase
    }

    func getPopularMovie() {
        isLoading.onNext(true)
        useCase
            .fetchPopularMoview()
            .asObservable()
            .subscribe(onNext: { [weak self] result in
                self?.movies.accept(result)
                self?.isLoading.onNext(false)
        }, onError: { [weak self] error in
            self?.errors.onNext(error)
            self?.isLoading.onNext(false)
        }).disposed(by: disposeBag)
    }
}
