//
//  MovieCollectionViewCell.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 24/03/23.
//

import UIKit
import Kingfisher

class MovieCollectionViewCell: UICollectionViewCell {

    private let posterImage: UIImageView = {
        let posterImage = UIImageView()
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.backgroundColor = .secondarySystemBackground
        posterImage.clipsToBounds = true
        posterImage.layer.cornerRadius = 6
        return posterImage
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initView()
    }
    
    private func initView() {
        self.contentView.layer.shadowColor = UIColor.gray.cgColor
        self.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.contentView.layer.shadowRadius = 5.0
        self.contentView.layer.shadowOpacity = 0.7

        self.contentView.addSubview(posterImage)
        posterImage
            .leadingAnchor
            .constraint(equalTo: self.contentView.leadingAnchor)
            .isActive = true
        posterImage
            .topAnchor
            .constraint(equalTo: self.contentView.topAnchor)
            .isActive = true
        posterImage
            .trailingAnchor
            .constraint(equalTo: self.contentView.trailingAnchor)
            .isActive = true
        posterImage
            .bottomAnchor
            .constraint(equalTo: self.contentView.bottomAnchor)
            .isActive = true
    }

    func setData(data: Movie) {
        if let posterUrl = URL(string: "https://image.tmdb.org/t/p/w500\(data.posterPath)") {
            posterImage.kf.setImage(with: posterUrl)
        }
    }
}
