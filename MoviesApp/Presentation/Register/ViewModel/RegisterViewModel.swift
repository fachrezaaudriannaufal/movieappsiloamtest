//
//  RegisterViewModel.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift

final class RegisterViewModel {
    private let useCase: UserUseCase
    private let disposeBag = DisposeBag()
    let registerStatus = PublishSubject<Bool>()
    
    init(useCase: UserUseCase) {
        self.useCase = useCase
    }

    func doRegister(fullname: String,
                    email: String,
                    password: String,
                    dateBirth: Date,
                    profileImage: Data) {
        useCase
            .register(fullname: fullname,
                      email: email,
                      password: password,
                      dateBirth: dateBirth,
                      profileImage: profileImage,
                      completion: { [weak self] result in
                guard let self = self else { return }
                switch result {
                case let .success(data):
                    if let data = data {
                        self.registerStatus.onNext(data)
                    }
                case let .failure(error):
                    self.registerStatus.onError(error)
                }
            })
    }
}
