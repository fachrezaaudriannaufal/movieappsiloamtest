//
//  RegisterViewController.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import UIKit
import TextFieldEffects
import RxSwift
import RxCocoa


class RegisterViewController: UIViewController {
    @IBOutlet weak var profilePictureImageView: UIImageView!
    @IBOutlet weak var fullNameTextfield: YoshikoTextField!
    @IBOutlet weak var emailTextfield: YoshikoTextField!
    @IBOutlet weak var passwordTextfield: YoshikoTextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var registerBtn: UIButton!

    private let viewModel = RegisterViewModel(useCase: MoviewDIContainer.shared.makeMovieAccountUseCase())
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapProfilePicture(_:)))
        profilePictureImageView.addGestureRecognizer(tapGesture)
        profilePictureImageView.isUserInteractionEnabled = true
        
        fullNameTextfield.placeholder = "Full Name"
        fullNameTextfield.placeholderColor = .gray
        fullNameTextfield.borderSize = 1
        fullNameTextfield.inactiveBorderColor = .systemGray3
        fullNameTextfield.placeholderFontScale = 1.2
        fullNameTextfield.activeBorderColor = .systemRed
        fullNameTextfield.borderStyle = .line
        fullNameTextfield.layer.cornerRadius = 8

        emailTextfield.placeholder = "Email"
        emailTextfield.placeholderColor = .gray
        emailTextfield.borderSize = 1
        emailTextfield.inactiveBorderColor = .systemGray3
        emailTextfield.placeholderFontScale = 1.2
        emailTextfield.activeBorderColor = .systemRed
        emailTextfield.borderStyle = .line
        emailTextfield.layer.cornerRadius = 8
        
        passwordTextfield.placeholder = "Password"
        passwordTextfield.placeholderColor = .gray
        passwordTextfield.borderSize = 1
        passwordTextfield.inactiveBorderColor = .systemGray3
        passwordTextfield.placeholderFontScale = 1.2
        passwordTextfield.activeBorderColor = .systemRed
        passwordTextfield.borderStyle = .line
        passwordTextfield.layer.cornerRadius = 8

        hideKeyboardWhenTappedAround()
        bindViewModel()
        bindRx()
    }

    func bindViewModel() {
        viewModel
            .registerStatus
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] result in
                guard let self = self else { return }
                AlertView.showAlertBox(title: "Berhasil", message: "Register Berhasil", handler: { [weak self] _ in
                    self?.dismiss(animated: true, completion: nil)
                }).present(on: self)
            }, onError: { [weak self] error in
                guard let self = self else { return }
                AlertView.showAlertBox(title: "Error", message: "Register Gagal", handler: { [weak self] _ in
                    self?.dismiss(animated: true, completion: nil)
                }).present(on: self)
            }).disposed(by: disposeBag)
    }
    
    func bindRx() {
        registerBtn.rx
            .tap
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }

                if let fullnameTextfield = self.fullNameTextfield.text,
                    fullnameTextfield.isEmpty {
                    AlertView.showAlertBox(title: "Warning", message: "Fullanme can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                
                if self.fullNameTextfield.activeBorderColor == .systemRed {
                    AlertView.showAlertBox(title: "Warning", message: "Fullanme must be 6 characters", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }

                if let emailTextfield = self.emailTextfield.text,
                   emailTextfield.isEmpty {
                    AlertView.showAlertBox(title: "Warning", message: "Email can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                
                if self.emailTextfield.activeBorderColor == .systemRed {
                    AlertView.showAlertBox(title: "Warning", message: "Please check email format", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }

                if let passwordTextfield = self.passwordTextfield.text,
                    passwordTextfield.isEmpty {
                    AlertView.showAlertBox(title: "Warning", message: "Password can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                
                if self.passwordTextfield.activeBorderColor == .systemRed {
                    AlertView.showAlertBox(title: "Warning", message: "Password must be 6 characters", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                

                if self.profilePictureImageView.image == nil {
                    AlertView.showAlertBox(title: "Warning", message: "Image can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }

                self.viewModel.doRegister(fullname: self.fullNameTextfield.text ?? "",
                                          email: self.emailTextfield.text ?? "",
                                          password: self.passwordTextfield.text ?? "",
                                          dateBirth: self.datePicker.date,
                                          profileImage: self.profilePictureImageView.image?.pngData() ?? Data())
        }).disposed(by: disposeBag)
        
        emailTextfield.rx
            .controlEvent([.editingChanged])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                let isEmailValid = self.emailTextfield.text?.isEmailFormatIsValid() ?? false
                self.emailTextfield.activeBorderColor = isEmailValid ? .systemGreen : .systemRed
            }).disposed(by: disposeBag)
        
        passwordTextfield.rx
            .controlEvent([.editingChanged])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                let isPasswordValid = self.passwordTextfield.text?.count ?? 0 > 6
                self.passwordTextfield.activeBorderColor =  isPasswordValid ? .systemGreen : .systemRed
            }).disposed(by: disposeBag)
        
        fullNameTextfield.rx
            .controlEvent([.editingChanged])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                let isFullnameValid = self.fullNameTextfield.text?.count ?? 0 > 6
                self.fullNameTextfield.activeBorderColor =  isFullnameValid ? .systemGreen : .systemRed
            }).disposed(by: disposeBag)
    }

    @IBAction func didTapProfilePicture(_ sender: UITapGestureRecognizer) {
        let pickerVC = UIImagePickerController()
        pickerVC.sourceType = .photoLibrary
        pickerVC.delegate = self
        pickerVC.allowsEditing = true
        self.present(pickerVC, animated: true)
    }
}

extension RegisterViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            profilePictureImageView.image = image
            picker.dismiss(animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}
