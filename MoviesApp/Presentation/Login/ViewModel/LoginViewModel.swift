//
//  LoginViewModel.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import Foundation
import RxSwift
import RxCocoa

final class LoginViewModel {
    private let useCase: UserUseCase
    private let disposeBag = DisposeBag()
    let loginAccount = PublishSubject<UserEntities>()
    
    init(useCase: UserUseCase) {
        self.useCase = useCase
    }

    func login(email: String, password: String) {
        useCase
            .login(email: email, password: password)
            .asObservable()
            .subscribe(onNext: { [weak self] result in
                self?.loginAccount.onNext(result)
            }, onError: { [weak self] error in
                self?.loginAccount.onError(error)
            }).disposed(by: disposeBag)
    }
}
