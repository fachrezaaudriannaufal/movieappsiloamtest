//
//  LoginViewController.swift
//  MoviesApp
//
//  Created by Fachreza's Macbook Pro on 23/03/23.
//

import UIKit
import TextFieldEffects
import RxCocoa
import RxSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextfield: YoshikoTextField!
    @IBOutlet weak var passwordTextfield: YoshikoTextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!

    private let viewModel = LoginViewModel(useCase: MoviewDIContainer.shared.makeMovieAccountUseCase())
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextfield.placeholder = "Email"
        emailTextfield.placeholderColor = .gray
        emailTextfield.borderSize = 1
        emailTextfield.inactiveBorderColor = .systemGray3
        emailTextfield.placeholderFontScale = 1.2
        emailTextfield.activeBorderColor = .systemRed
        emailTextfield.borderStyle = .line
        emailTextfield.layer.cornerRadius = 8
        
        passwordTextfield.placeholder = "Password"
        passwordTextfield.placeholderColor = .gray
        passwordTextfield.borderSize = 1
        passwordTextfield.inactiveBorderColor = .systemGray3
        passwordTextfield.placeholderFontScale = 1.2
        passwordTextfield.activeBorderColor = .systemRed
        passwordTextfield.borderStyle = .line
        passwordTextfield.layer.cornerRadius = 8
        
        hideKeyboardWhenTappedAround()
        bindViewModel()
        bindRx()
    }

    func bindViewModel() {
        viewModel
            .loginAccount
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { _ in
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
                    UIApplication.shared.windows.first?.rootViewController = viewController
                }
        }, onError: { error in
            AlertView.showAlertBox(title: "Warning", message: "Failed to login", handler: { [weak self] _ in
                self?.dismiss(animated: true, completion: nil)
            }).present(on: self)
        }).disposed(by: disposeBag)
    }

    func bindRx() {
        emailTextfield.rx
            .controlEvent([.editingChanged])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                let isEmailValid = self.emailTextfield.text?.isEmailFormatIsValid() ?? false
                self.emailTextfield.activeBorderColor = isEmailValid ? .systemGreen : .systemRed
            }).disposed(by: disposeBag)
        
        passwordTextfield.rx
            .controlEvent([.editingChanged])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                let isPasswordValid = self.passwordTextfield.text?.count ?? 0 > 6
                self.passwordTextfield.activeBorderColor =  isPasswordValid ? .systemGreen : .systemRed
            }).disposed(by: disposeBag)
        
        loginBtn.rx
            .controlEvent([.touchUpInside])
            .asDriver()
            .drive(onNext: { [weak self] in
                guard let self = self else { return }
                if let emailTextfield = self.emailTextfield.text,
                   emailTextfield.isEmpty {
                    AlertView.showAlertBox(title: "Warning", message: "Email can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                
                if self.emailTextfield.activeBorderColor == .systemRed {
                    AlertView.showAlertBox(title: "Warning", message: "Please check email format", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }

                if let passwordTextfield = self.passwordTextfield.text,
                    passwordTextfield.isEmpty {
                    AlertView.showAlertBox(title: "Warning", message: "Password can't empty", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }
                
                if self.passwordTextfield.activeBorderColor == .systemRed {
                    AlertView.showAlertBox(title: "Warning", message: "Password must be 6 characters", handler: { [weak self] _ in
                        self?.dismiss(animated: true, completion: nil)
                    }).present(on: self)
                    return
                }

                self.viewModel.login(email: self.emailTextfield.text ?? "", password: self.passwordTextfield.text ?? "")
        }).disposed(by: disposeBag)
        
        registerBtn.rx
            .controlEvent([.touchUpInside])
            .asDriver()
            .drive(onNext: { [weak self] in
                let storyboard = UIStoryboard(name: "Register", bundle: nil)
                let registerVC = storyboard.instantiateViewController(withIdentifier: "RegisterViewController")
                self?.navigationController?.pushViewController(registerVC, animated: true)
        }).disposed(by: disposeBag)
    }
}


extension String {
    func isEmailFormatIsValid() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
